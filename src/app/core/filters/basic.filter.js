(function() {
    'use strict';

    angular
        .module('app.core')
        .filter('toTrusted', toTrustedFilter)
        .filter('htmlToPlaintext', htmlToPlainTextFilter)
        .filter('nospace', nospaceFilter)
        .filter('humanizeDoc', humanizeDocFilter)
        .filter('range', rangeFilter)
        .filter('reverse', reverseFilter)
        .filter('isEmpty', isEmptyFilter)
        .filter('duration', durationFilter)
        .filter('money', moneyFilter)
        .filter('status', statusFilter)
        .filter('currentStatus', currentStatusFilter)
        .filter('services', servicesFilter)
        .filter('phone', phoneFilter)
        .filter('car', carFilter)
        .filter('address', addressFilter)
        .filter('paymentStatus', paymentStatusFilter)
        .filter('paymentMethod', paymentMethodFilter)
        .filter('carPlate', carPlateFilter)

    /** @ngInject */
    function toTrustedFilter($sce) {
        return function(value) {
            return $sce.trustAsHtml(value);
        };
    }

    /** @ngInject */
    function htmlToPlainTextFilter() {
        return function(text) {
            return String(text).replace(/<[^>]+>/gm, '');
        };
    }

    /** @ngInject */
    function nospaceFilter() {
        return function(value) {
            return (!value) ? '' : value.replace(/ /g, '');
        };
    }

    /** @ngInject */
    function humanizeDocFilter() {
        return function(doc) {
            if (!doc) {
                return;
            }
            if (doc.type === 'directive') {
                return doc.name.replace(/([A-Z])/g, function($1) {
                    return '-' + $1.toLowerCase();
                });
            }
            return doc.label || doc.name;
        };
    }

    /** @ngInject */
    function rangeFilter() {
        return function(input, min, max) {
            min = parseInt(min); //Make string input int
            max = parseInt(max);
            for (var i = min; i <= max; i++)
                input.push(i);
            return input;
        };
    }
    /** @ngInject */
    function reverseFilter() {
        return function(items) {
            return items.slice().reverse();
        };
    }

    /** @ngInject */
    function isEmptyFilter() {
        return function(value) {
            return _.isEmpty(value);
        };
    }

    /** @ngInject */
    function durationFilter() {
        return function(value) {

            if(!value)
                return '';
            
            var hours = _.toInteger(value / 60);
            var minutes = value % 60;

            var duration = '';

            if (hours > 1) {
                duration = hours.toString() + ' horas';
            } else if (hours === 1) {
                duration = hours.toString() + ' hora';
            }

            if (minutes > 0 && hours > 0) {
                duration += ' e ' + minutes.toString() + ' minutos';
            } else if (minutes > 0 && hours == 0) {
                duration = minutes.toString() + ' minutos';
            }

            if (_.isEmpty(duration))
                return 'Não definido';
            return duration;
        };
    }

    function moneyFilter() {
        return function(value) {
            if (value && value > 0) {
                value = value.toString();

                return Number([value.slice(0, value.length - 2), '.', value.slice(value.length - 2)].join(''));
            }
            return 0;
        }
    }

    function statusFilter(STATUSES) {
        return function(value) {
            switch (value) {
                case STATUSES.QUOTE_PENDING:
                    return "Orçamento Pendente";
                case STATUSES.QUOTE_COMPLETED:
                    return "Orçamento Completo"
                case STATUSES.APPOINTMENT_BOOKED:
                    return "Agendamento realizado";
                case STATUSES.MECHANIC_ON_THE_WAY:
                    return "Mecânico em atendimento"
                case STATUSES.APPOINTMENT_COMPLETED:
                    return "Agendamento concluído"
            }
        }
    }

    function currentStatusFilter(STATUSES) {
        return function(value) {

            if (!value || _.isEmpty(value))
                return 'Não definido';

            var statuses = [];
            for (var key in value) {
                switch (key) {
                    case STATUSES.QUOTE_PENDING:
                        value[key].name = "Orçamento Pendente";
                        break;
                    case STATUSES.QUOTE_COMPLETED:
                        value[key].name = "Orçamento Completo"
                        break;
                    case STATUSES.APPOINTMENT_BOOKED:
                        value[key].name = "Agendamento realizado";
                        break;
                    case STATUSES.MECHANIC_ON_THE_WAY:
                        value[key].name = "Mecânico em atendimento"
                        break;
                    case STATUSES.APPOINTMENT_COMPLETED:
                        value[key].name = "Agendamento concluído"
                        break;
                }
            }
            return _.last(_.orderBy(value, ['date'])).name;
        }
    }

    function servicesFilter() {
        return function(value) {
            var s = '';

            if (value) {
                _.forEach(value, function(service, index) {
                    if (service) {
                        s += service.name;
                        if (index < value.length - 1) {
                            s += '<br>';
                        }
                    }
                });
            }

            return s;
        }
    }

    function phoneFilter() {
        return function(value) {
            var str = value + '';
            str = str.replace(/\D/g, '');
            if (str.length === 11) {
                str = str.replace(/^(\d{2})(\d{5})(\d{4})/, '($1) $2-$3');
            } else {
                str = str.replace(/^(\d{2})(\d{4})(\d{4})/, '($1) $2-$3');
            }
            return str;
        };
    }

    function carFilter() {
        return function(value) {
            if(!value || !value.make || !value.model || !value.year)
                return '';
            return value.make.name + ' ' + value.model.name + ' ' + value.year;
        };
    }

    function addressFilter() {
        return function(value) {

            if(!value)
                return '';

            var value_formatted = '';

            if (value.number && value.complement)
                value_formatted = value.street + ', ' + value.number + ' - ' + value.complement + ' - ' + value.district + ' - ' + value.city + ' / ' + value.state;
            else if (value.number && !value.complement) {
                value_formatted = value.street + ', ' + value.number + ' - ' + value.district + ' - ' + value.city + ' / ' + value.state;
            } else if (!value.number && value.complement) {
                value_formatted = value.street + ' - ' + value.complement + ' - ' + value.district + ' - ' + value.city + ' / ' + value.state;
            } else if(value.street) {
                value_formatted = value.street + ' - ' + value.district + ' - ' + value.city + ' / ' + value.state;
            } 

            return value_formatted;
        }
    }

    function paymentStatusFilter() {
        return function(value) {
            switch (value) {
                case 'CREATED':
                    return 'Criado';
                case 'WAITING':
                    return 'Aguardando';
                case 'IN_ANALYSIS':
                    return 'Em análise';
                case 'PRE_AUTHORIZED':
                    return 'Pré-autorizado';
                case 'AUTHORIZED':
                    return 'Autorizado';
                case 'CANCELLED':
                    return 'Cancelado';
                case 'REFUNDED':
                    return 'Reembolsado';
                case 'REVERSED':
                    return 'Revertido';
                case 'SETTLED':
                    return 'Estabelecido';
            }
        }
    }

    function paymentMethodFilter() {
        return function(value) {
            switch (value) {
                case 'CREDIT_CARD':
                    return 'Cartão de crédito';
                case 'BOLETO':
                    return 'Boleto bancário';
                case 'ONLINE_BANK_DEBIT':
                    return 'Débito Online';
            }
        }
    }

    function carPlateFilter() {
        return function(value) {
            if(!value)
                return '';
            return value.replace(/^(\w{3})(\d{4})/, '$1-$2');
        }
    }


})();
