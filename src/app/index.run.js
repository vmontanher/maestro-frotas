(function() {
    'use strict';

    angular
        .module('fuse')
        .run(runBlock);

    /** @ngInject */
    function runBlock($rootScope, $timeout, $state, $location, $window, editableOptions, editableThemes, amMoment, AuthService, UserService) {

        //Change locale moment
        amMoment.changeLocale('pt-br');

        var lastUrl = null;
        // Activate loading indicator
        var stateChangeStartEvent = $rootScope.$on('$stateChangeStart', function() {
            $rootScope.loadingProgress = true;
            if (lastUrl && AuthService.isLoggedIn()) {
                $location.path(lastUrl);
                lastUrl = null;
            }

        });

        // De-activate loading indicator
        var stateChangeSuccessEvent = $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            $timeout(function() {
                $rootScope.loadingProgress = false;
            });
        });

        // Store state in the root scope for easy access
        $rootScope.state = $state;


        //Back to last url
        $rootScope.goBack = function () {
            $window.history.back();
        };

        // Cleanup
        $rootScope.$on('$destroy', function() {
            stateChangeStartEvent();
            stateChangeSuccessEvent();
        });


        // Go to Home if user not authed
        $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
            if (error === "AUTH_REQUIRED") {
                lastUrl = $state.href(toState.name, toParams);
                $state.go('app.login');
            }
        });

        // Account-related scope initialization
        $rootScope.account = {};

        // Listener Authentication
        AuthService.firebaseAuthObject.$onAuthStateChanged(function(authData) {
            $rootScope.account.authData = authData;

            if (authData) {
                $rootScope.account.profile = UserService.getUser(authData.uid);                
            } else {
                if ($rootScope.account.profile) {
                    $rootScope.account.profile.$destroy();
                }
                delete $rootScope.account.profile
            }
        });

        //Customize editable-text
        editableOptions.theme = 'default';
        editableThemes['default'] = {
            formTpl: '<form class="editable-wrap"></form>',
            noformTpl: '<span class="editable-wrap"></span>',
            controlsTpl: '<md-input-container class="editable-controls" ng-class="{\'md-input-invalid\': $error}"></md-input-container>',
            inputTpl: '',
            errorTpl: '<div ng-messages="{message: $error}"><div class="editable-error" ng-message="message">{{$error}}</div></div>',
            buttonsTpl: '<span class="editable-buttons"></span>',
            submitTpl: '<md-button type="submit" class="md-icon-button"><md-icon md-font-icon="icon-checkbox-marked-circle" class="green-600-fg"></md-icon></md-button>',
            cancelTpl: '<md-button type="button" class="md-icon-button md-warn" ng-click="$form.$cancel()"><md-icon md-font-icon="icon-close-circle"></md-icon></md-button>'
        };
    }
})();
