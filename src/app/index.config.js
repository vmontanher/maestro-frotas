(function() {
    'use strict';

    angular
        .module('fuse')
        .config(config);

    /** @ngInject */
    function config($mdDateLocaleProvider, $translateProvider, ENV) {

        // angular-translate configuration
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: '{part}/i18n/{lang}.json'
        });
        $translateProvider.preferredLanguage('pt-br');
        $translateProvider.useSanitizeValueStrategy('sanitize');

        if (ENV == 'PROD') {
            //Initialize Firebase PROD
            var config = {
                apiKey: "AIzaSyBcWz1GMixExkhbIYxaVXDWJ-mdl1TTC-c",
                authDomain: "maestrofrotas-dd488.firebaseapp.com",
                databaseURL: "https://maestrofrotas-dd488.firebaseio.com",
                projectId: "maestrofrotas-dd488",
                storageBucket: "maestrofrotas-dd488.appspot.com",
                messagingSenderId: "552063252456"
            };
        } else {
            // Initialize Firebase DEV
            var config = {
                apiKey: "AIzaSyA6Gm0cgv7W13CKwsmQ4LVqTCX9ggBhnb4",
                authDomain: "nostrinquesb2b-dev.firebaseapp.com",
                databaseURL: "https://nostrinquesb2b-dev.firebaseio.com",
                projectId: "nostrinquesb2b-dev",
                storageBucket: "nostrinquesb2b-dev.appspot.com",
                messagingSenderId: "811429192736"
            };

        }
        firebase.initializeApp(config);

        $mdDateLocaleProvider.firstDayOfWeek = 1;
        $mdDateLocaleProvider.shortDays = ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'];
        $mdDateLocaleProvider.parseDate = function(dateString) {
            var m = moment(dateString, 'DD/MM/YYYY', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        };

        $mdDateLocaleProvider.formatDate = function(date) {
            return date ? moment(date).format('DD/MM/YYYY') : '';
        };

        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "date-pre": function(date) {
                return moment(date, 'DD/MM/YYYY - HH:mm');
            },
            "date-asc": function(a, b) {
                return (a.isBefore(b) ? -1 : (a.isAfter(b) ? 1 : 0));
            },
            "date-desc": function(a, b) {
                return (a.isBefore(b) ? 1 : (a.isAfter(b) ? -1 : 0));
            }
        });
    }

})();
