(function() {
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
        .module('fuse', [

            // Firebase,
            'firebase',

            // Input Masks
            'ui.utils.masks',

            // Angular Moment,
            'angularMoment',

            // Core
            'app.core',

            // Navigation
            'app.navigation',

            // Toolbar
            'app.toolbar',

            // Quick panel
            'app.quick-panel',

            //'app.chat',

            // Pages
            'app.pages'
        ]);
})();