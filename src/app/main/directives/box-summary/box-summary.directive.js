(function() {
    'use strict';

    angular
        .module('app.core')
        .directive('boxSummary', boxSummaryDirective);

    /** @ngInject */
    function boxSummaryDirective($document) {

        return {
            restrict: 'EA',
            transclude: true,
            templateUrl: 'app/main/directives/box-summary/box-summary.html'
        };
    }
})();