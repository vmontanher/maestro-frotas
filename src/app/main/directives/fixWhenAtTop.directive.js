(function() {
    'use strict';

    angular
        .module('app.core')
        .directive('fixWhenAtTop', fixWhenAtTopDirective);

    /** @ngInject */
    function fixWhenAtTopDirective($document) {

        return {

            restrict: 'A',
            link: function(scope, element, attrs) {
                var offsetTop = element.offset().top; // get element's top relative to the document

                var padding = 60;

                var $doc = angular.element('#content');

                $doc.bind('ps-scroll-y', function(e) {
                    if ($doc.scrollTop() >= offsetTop / 2 + padding) {
                        element.css('position', 'relative').css('top', ($doc.scrollTop() - offsetTop / 2 - padding) + 'px');
                    } else {
                        element.css('position', 'relative').css('top', '0px');
                    }
                });
            }
        }
    }
})();