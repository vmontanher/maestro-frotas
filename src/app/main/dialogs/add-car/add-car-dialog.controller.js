(function() {
    'use strict';

    angular
        .module('app.pages.cars')
        .controller('AddCarDialogController', AddCarDialogController);

    /** @ngInject */
    function AddCarDialogController($mdDialog, $rootScope, Makes, CarService, Book) {
        var vm = this;

        // Data
        vm.makes = Makes;

        // Methods
        vm.getModelsByMake = getModelsByMake;
        vm.saveCar = saveCar;
        vm.closeDialog = closeDialog;

        //////////

        function getModelsByMake() {
            var make = vm.car.make;
            vm.models = CarService.getModelsByMake(make.$id);
        }

        function saveCar() {
            vm.plateError = null;

            CarService.getCarByPlate(vm.car.plate).$loaded(function(data) {
                if (!_.isEmpty(data)) {
                    vm.plateError = "Já existe um carro cadastrado com essa placa";
                    return;
                }
                var car = {
                    //Make
                    make: {
                        id: vm.car.make.$id,
                        name: vm.car.make.name
                    },
                    //Model
                    model: {
                        id: vm.car.model.$id,
                        name: vm.car.model.name
                    },
                    //Year
                    year: vm.car.year,
                    //Plate
                    plate: vm.car.plate
                };

                CarService.addCar(car).then(function(refKey) {
                    if (Book) {
                        CarService.getCar(refKey).$loaded(function(data) {
                            Book.car = data;
                            closeDialog();
                        });
                    } else {
                        closeDialog();
                    }

                });
            });

        }

        function closeDialog() {
            $mdDialog.hide();
        }




    }

})();
