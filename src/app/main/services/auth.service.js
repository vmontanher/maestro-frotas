(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('AuthService', AuthService);

    /** @ngInject */
    function AuthService($state, $firebaseAuth, firebaseDataService) {

        var firebaseAuthObject = $firebaseAuth();

        var service = {
            firebaseAuthObject: firebaseAuthObject,
            register: register,
            login: login,
            logout: logout,
            isLoggedIn: isLoggedIn,
            getAuth: getAuth
        };

        return service;

        ////////////

        function register(user) {
            return firebaseAuthObject.$createUserWithEmailAndPassword(user.email, user.password);
        }

        function login(user) {
            return firebaseAuthObject.$signInWithEmailAndPassword(user.email, user.password);
        }

        function logout() {
            firebaseAuthObject.$signOut().then(function() {
                $state.go('app.login');
            });
        }

        function isLoggedIn() {
            return firebaseAuthObject.$getAuth() ? true : false;
        }

        function getAuth() {
            return firebaseAuthObject.$getAuth();
        }

    }
})();
