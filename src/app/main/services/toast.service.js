(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('Toast', Toast);

    /** @ngInject */
    function Toast($mdToast) {

        var service = {
            show: show
        };

        return service;

        ////////////

        function show(text, position, delay) {
            var toast = $mdToast.simple()
                .textContent(text)
                .position(position || 'top right')
                .hideDelay(delay || 3000)

            $mdToast.show(toast);
        }

    }
})();