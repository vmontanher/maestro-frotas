(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('QuoteService', QuoteService);

    /** @ngInject */
    function QuoteService($q, $firebaseObject, $firebaseArray, firebaseDataService, AnalyticsService) {

        var newQuote = null;

        var service = {
            getQuotes: getQuotes,
            getQuote: getQuote,
            getQuotesCount: getQuotesCount,
            getQuotesThisWeek: getQuotesThisWeek,
            getQuotesLastWeek: getQuotesLastWeek,
            addQuote: addQuote,
            removeQuote: removeQuote
        };

        return service;

        ////////////

        function getQuotes() {
            return $firebaseArray(firebaseDataService.quotes);
        }

        function getQuote(quoteId) {
            return $firebaseObject(firebaseDataService.quotes.child(quoteId));
        }

        function getQuotesCount() {
            return $firebaseObject(firebaseDataService.quotes.child('count'));
        }

        function getQuotesThisWeek() {
            var start = Number(moment().startOf('isoWeek').format('x'));
            var end = _.now();

            return $firebaseArray(firebaseDataService.quotes.orderByChild("createdAt").startAt(start).endAt(end));
        }

        function getQuotesLastWeek() {
            var start = Number(moment().startOf('isoWeek').subtract(7, 'days').format('x'));
            var end = Number(moment().endOf('isoWeek').subtract(7, 'days').format('x'));

            return $firebaseArray(firebaseDataService.quotes.orderByChild("createdAt").startAt(start).endAt(end));
        }

        function addQuote(quote) {

            //Format before save 
            _.forEach(quote.services, function(service) {
                delete service.$id;
                delete service.$$hashKey;
                delete service.$priority;
            });

            var deffered = $q.defer();

            //Create quote Id
            var newQuoteKey = firebaseDataService.quotes.push().key;

            var update = {};

            //Update quote and user-orders relationship
            update['quotes/' + newQuoteKey] = quote;
            update['cars/' + quote.car.id + '/quotes/' + newQuoteKey] = true;

            firebaseDataService.root.update(update, function(error) {
                if (error) {
                    deffered.reject(error)
                } else {
                    deffered.resolve(newQuoteKey);
                }
            });

            return deffered.promise;
        }

        function removeQuote(quote) {

            var deffered = $q.defer();

            var update = {};

            update['quotes/' + quote.$id] = null;
            update['cars/' + quote.car.id + '/quotes/' + quote.$id] = null;

            firebaseDataService.root.update(update, function(error) {
                if (error) {
                    deffered.reject(error)
                } else {
                    deffered.resolve();
                }
            });

            return deffered.promise;
        }

    }
})();
