(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('firebaseDataService', firebaseDataService);

    /** @ngInject */
    function firebaseDataService($firebaseArray) {

        var root = firebase.database().ref();

        var service = {
            root: root,
            analytics: root.child('analytics'),
            carMakes: root.child('car-makes'),
            carModels: root.child('car-models'),
            carModelServices: root.child('car-model-services'),
            cars: root.child('cars'),
            quotes: root.child('quotes'),
            orders: root.child('orders'),
            users: root.child('users'),
            roles: root.child('roles'),
            services: root.child('services'),
            statuses: root.child('statuses'),
            customers: root.child('customers')
        };

        return service;
    }
})();