(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('RoleService', RoleService);

    /** @ngInject */
    function RoleService($firebaseObject, $firebaseArray, firebaseDataService) {

        var service = {
            isMaster: isMaster
        };

        return service;

        ////////////

        function isMaster(uid) {
            return $firebaseObject(firebaseDataService.roles.child('master/'+uid));
        }
    }
})();
