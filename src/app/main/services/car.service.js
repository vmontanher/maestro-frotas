(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('CarService', CarService);

    /** @ngInject */
    function CarService($q, $rootScope, $firebaseArray, $firebaseObject, firebaseDataService, AnalyticsService) {

        var service = {
            getCars: getCars,
            getCar: getCar,
            getCarByPlate: getCarByPlate,
            getCarsCount: getCarsCount,
            getMakes: getMakes,
            getModelsByMake: getModelsByMake,
            getServicesByModel: getServicesByModel,
            getAllServices: getAllServices,
            addCar: addCar,
            removeCar: removeCar,
            addService: addService
        };

        return service;

        ////////////

        function getCars() {
            return $firebaseArray(firebaseDataService.cars);
        }

        function getCar(carId) {
            return $firebaseObject(firebaseDataService.cars.child(carId));
        }

        function getCarByPlate(plate) {
            return $firebaseArray(firebaseDataService.cars.orderByChild('plate').equalTo(plate));
        }

        function getCarsCount() {
            return $firebaseObject(firebaseDataService.cars.child('count'));
        }

        function getMakes() {
            return $firebaseArray(firebaseDataService.carMakes);
        }

        function getModelsByMake(makeId) {
            return $firebaseArray(firebaseDataService.carModels.child(makeId));
        }

        function getServicesByModel(modelId) {
            return $firebaseArray(firebaseDataService.carModelServices.child(modelId));
        }

        function getAllServices() {
            return $firebaseArray(firebaseDataService.services);
        }

        function addCar(car) {

            var deffered = $q.defer();

            //Create car Id
            var newCarKey = firebaseDataService.cars.push().key;

            var update = {};

            //Update cars and company-cars relationship
            update['cars/' + newCarKey] = car;

            firebaseDataService.root.update(update, function(error) {
                if (error) {
                    deffered.reject(error)
                } else {
                    deffered.resolve(newCarKey);
                }
            });

            return deffered.promise;
        }

        function removeCar(car) {

            var deffered = $q.defer();

            var update = {};

            //Remove car data
            update['cars/' + car.$id] = null;

            //Remove quote's car
            if (car.quotes) {
                var quotesKey = _.keys(car.quotes);
                _.forEach(quotesKey, function(key) {
                    update['quotes/' + key] = null;
                });
            }

            firebaseDataService.root.update(update, function(error) {
                if (error) {
                    deffered.reject(error)
                } else {
                    deffered.resolve();
                }
            });

            return deffered.promise;

        }

        function addService(car, service) {

            var deffered = $q.defer();

            _.forEach(service.parts, function(part) {
                delete part.$$hashKey;
            });

            var update = {};

            update['car-model-services/' + car.model.id + '/' + service.id] = {
                laborTime: service.laborTime,
                parts: service.parts
            };

            firebaseDataService.root.update(update, function(error) {
                if (error) {
                    deffered.reject(error)
                } else {
                    deffered.resolve();
                }
            });

            return deffered.promise;
        }


    }
})();
