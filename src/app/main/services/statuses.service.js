(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('StatusesService', StatusesService);

    /** @ngInject */
    function StatusesService($firebaseObject, $firebaseArray, firebaseDataService) {

        var service = {
            getStatus: getStatus,
            getStatuses: getStatuses
        };

        return service;

        ////////////

        function getStatus(statusId) {
            return $firebaseObject(firebaseDataService.statuses.child(statusId));
        }

        function getStatuses() {
            return $firebaseArray(firebaseDataService.statuses);
        }
    }
})();
