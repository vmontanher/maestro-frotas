(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('EmailService', EmailService);

    /** @ngInject */
    function EmailService($http, $filter, AuthService, API) {

        var service = {
            sendWelcome: sendWelcome,
            sendQuote: sendQuote
        };

        return service;

        //////////////////

        function sendWelcome(user) {
            var data = {
                to: user.email,
                name: user.fullname.split(' ')[0]
            };
            return AuthService.getAuth().getToken(true).then(function(token) {
                return $http.post(API.baseURL + '/mail/welcome', data, {
                    headers: {
                        token: token
                    }
                });
            });
        }

        function sendQuote(user, quote) {
            var data = {
                quoteId: quote.$id || quote.id,
                to: user.email,
                name: user.fullname.split(' ')[0],
                car: $filter('services')(quote.car),
                services: $filter('services')(quote.services),
                labor_time: $filter('duration')(quote.labor_time),
                total: $filter('currency')(quote.total, 'R$')
            };

            return AuthService.getAuth().getToken(true).then(function(token) {
                return $http.post(API.baseURL + '/mail/quote', data, {
                    headers: {
                        token: token
                    }
                });
            });
        }

    }


})();
