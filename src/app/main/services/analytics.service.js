(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('AnalyticsService', AnalyticsService);

    /** @ngInject */
    function AnalyticsService($firebaseObject, firebaseDataService) {

        var service = {
            getAnalytics: getAnalytics,
            increaseCount: increaseCount,
            decreaseCount: decreaseCount,
            increaseAmount: increaseAmount,
            decreaseAmount: decreaseAmount
        };

        return service;

        ////////////

        function getAnalytics() {
            return $firebaseObject(firebaseDataService.analytics);
        }

        function increaseCount(dataName, quantity) {
            firebaseDataService.root.child('analytics/count/' + dataName).transaction(function(count) {
                return count + (quantity || 1);
            });
        }

        function decreaseCount(dataName, quantity) {
            firebaseDataService.root.child('analytics/count/' + dataName).transaction(function(count) {
                return count - (quantity || 1);
            });
        }

        function increaseAmount(dataName, value) {
            firebaseDataService.root.child('analytics/amount/' + dataName).transaction(function(amount) {
                return _.round(amount + (value || 0), 2);
            });
        }

        function decreaseAmount(dataName, value) {
            firebaseDataService.root.child('analytics/amount/' + dataName).transaction(function(amount) {
                return _.round(amount - (value || 0), 2);
            });
        }
    }
})();
