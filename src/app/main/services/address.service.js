(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('AddressService', AddressService);

    /** @ngInject */
    function AddressService($http, $firebaseArray, firebaseDataService) {

        var service = {
            getAddressByZip: getAddressByZip,
            formatAddressName: formatAddressName
        };

        return service;

        ////////////

        function getAddressByZip(zip) {
            return $http.get('https://viacep.com.br/ws/' + zip + '/json/')
                .then(function(response) {
                    return response.data;
                }).catch(function(error) {
                    return error
                });
        }

        function formatAddressName(address) {
            var address_formatted = '';

            if (address.number && address.complement)
                address_formatted = address.street + ', ' + address.number + ' - ' + address.complement + ' - ' + address.district + ' - ' + address.city + ' / ' + address.state;
            else if (address.number && !address.complement) {
                address_formatted = address.street + ', ' + address.number + ' - ' + address.district + ' - ' + address.city + ' / ' + address.state;
            } else if (!address.number && address.complement) {
                address_formatted = address.street + ' - ' + address.complement + ' - ' + address.district + ' - ' + address.city + ' / ' + address.state;
            } else {
                address_formatted = address.street + ' - ' + address.district + ' - ' + address.city + ' / ' + address.state;
            }

            return address_formatted;
        }

    }
})();