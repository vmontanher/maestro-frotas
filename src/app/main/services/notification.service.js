(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('NotificationService', NotificationService);

    /** @ngInject */
    function NotificationService($http, $filter) {

        var service = {
            notifyQuote: notifyQuote,
            notifyOrder: notifyOrder
        };

        return service;

        ////////////

        function notifyQuote(quote) {

            var services = formatServicesToString(quote.services);

            var data = {
                token: 'xoxp-62507827458-62497301255-75110031233-6a1f8679a2',
                channel: 'maestro-orcamentos',
                username: 'Notificação - Orçamento',
                icon_emoji: ":loudspeaker:",
                attachments: [{
                    fallback: "Novo Orçamento",
                    color: "good",
                    author_name: 'Maestro Frotas',
                    fields: [{
                        title: "Carro",
                        value: $filter('car')(quote.car),
                        short: false
                    }, {
                        title: "Serviços",
                        value: services,
                        short: false
                    }, {
                        title: "Local",
                        value: $filter('address')(quote.address),
                        short: false
                    }, {
                        title: "Data do pedido",
                        value: moment(quote.createdAt).format('LLLL'),
                        short: false
                    }],
                    footer: quote.id,
                    footer_icon: ":calendar:"
                }]

            };

            if(quote.problems) {
                data.attachments[0].fields.push({
                    title: "Problemas",
                    value: quote.problems,
                    short: false
                });
            }

            var request = {
                url: 'https://hooks.slack.com/services/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',
                method: 'POST',
                dataType: 'json',
                data: data,
                headers: {
                    'Content-Type': undefined
                }
            };
            
            return $http(request)
                .then(notifyQuoteComplete);

            function notifyQuoteComplete(response) {
                return response.data;
            }
        }

        function notifyOrder(order) {

            var services = formatServicesToString(order.services);

            var data = {
                token: 'xoxp-62507827458-62497301255-75110031233-6a1f8679a2',
                channel: 'maestro-agendamentos',
                username: 'Notificação - Agendamento',
                icon_emoji: ":tada:",
                attachments: [{
                    fallback: "Novo Agendamento",
                    color: "good",
                    author_name: 'Maestro Frotas',
                    fields: [{
                        title: "Carro",
                        value: $filter('car')(order.car),
                        short: false
                    }, {
                        title: "Serviços",
                        value: services,
                        short: false
                    }, {
                        title: "Local",
                        value: $filter('address')(order.address),
                        short: false
                    }, {
                        title: "Data do Agendamento",
                        value: moment(order.appointment_date).format('LLLL'),
                        short: false
                    }],
                    footer: order.id || order.$id,
                    footer_icon: ":calendar:"
                }]
            };

            if(order.problems) {
                data.attachments[0].fields.push({
                    title: "Problemas",
                    value: order.problems,
                    short: false
                });
            }

            var request = {
                url: 'https://hooks.slack.com/services/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',
                method: 'POST',
                dataType: 'json',
                data: data,
                headers: {
                    'Content-Type': undefined
                }
            };

            return $http(request)
                .then(notifyOrderComplete);

            function notifyOrderComplete(response) {
                return response.data;
            }
        }

        function formatServicesToString(services) {
            var s = '';

            if (services) {
                _.forEach(services, function(service, index) {
                    s += service.name;
                    if (index < services.length - 1) {
                        s += '\n';
                    }
                });
            }

            return s;
        }


    }
})();
