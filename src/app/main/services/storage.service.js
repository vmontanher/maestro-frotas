(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('StorageService', StorageService);

    /** @ngInject */
    function StorageService($http, $q) {

        var storageRef = firebase.storage().ref();

        var service = {
            uploadFromUrl: uploadFromUrl
        };

        return service;

        ////////////

        function uploadFromUrl(url, metadata, path) {

            var deferred = $q.defer();

            $http({
                    url: url,
                    method: 'GET',
                    responseType: 'blob'
                })
                .then(function(response) {
                    var file = response.data;
                    var uploadTask = storageRef.child(path).put(file, metadata);
                    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
                        function(snapshot) {
                            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                            switch (snapshot.state) {
                                case firebase.storage.TaskState.PAUSED: // or 'paused'
                                    break;
                                case firebase.storage.TaskState.RUNNING: // or 'running'
                                    break;
                            }
                        },
                        function(error) {
                            switch (error.code) {
                                case 'storage/unauthorized':
                                    deferred.reject(null);
                                    break;

                                case 'storage/canceled':
                                    deferred.reject(null);
                                    break;
                                case 'storage/unknown':
                                    deferred.reject(null);
                                    break;
                            }
                        },
                        function() {
                            // Upload completed successfully, now we can get the download URL
                            deferred.resolve(uploadTask.snapshot.downloadURL);
                        });
                }).catch(function(error) {
                    deferred.reject(null);
                });

            return deferred.promise;
        }

    }
})();
