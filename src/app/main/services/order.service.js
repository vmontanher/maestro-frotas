(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('OrderService', OrderService);

    /** @ngInject */
    function OrderService($q, $firebaseObject, $firebaseArray, firebaseDataService, AnalyticsService) {

        var service = {
            getOrders: getOrders,
            getOrder: getOrder,
            getOrdersCount: getOrdersCount,
            getOrdersTotal: getOrdersTotal,
            getOrdersThisMonth: getOrdersThisMonth,
            getOrdersThisWeek: getOrdersThisWeek,
            getOrdersLastWeek: getOrdersLastWeek,
            addOrder: addOrder,
            removeOrder: removeOrder
        };

        return service;

        ////////////

        function getOrders() {
            return $firebaseArray(firebaseDataService.orders);
        }

        function getOrder(orderId) {
            return $firebaseObject(firebaseDataService.orders.child(orderId));
        }

        function getOrdersCount() {
            return $firebaseObject(firebaseDataService.orders.child('count'));
        }

        function getOrdersTotal() {
            return $firebaseObject(firebaseDataService.orders.child('total'));
        }

        function getOrdersThisMonth() {
            var currentYear = moment().format('YYYY');
            var currentMonth = moment().format('M');
            var startDate = moment([currentYear, currentMonth - 1]);

            var endDate = Number(moment(startDate).endOf('month').format('x'));
            startDate = Number(startDate.format('x'));

            return $firebaseArray(firebaseDataService.orders.orderByChild("createdAt").startAt(startDate).endAt(endDate));
        }

        function getOrdersThisWeek() {
            var start = Number(moment().startOf('isoWeek').format('x'));
            var end = _.now();

            return $firebaseArray(firebaseDataService.orders.orderByChild("createdAt").startAt(start).endAt(end));
        }

        function getOrdersLastWeek() {
            var start = Number(moment().startOf('isoWeek').subtract(7, 'days').format('x'));
            var end = Number(moment().endOf('isoWeek').subtract(7, 'days').format('x'));

            return $firebaseArray(firebaseDataService.orders.orderByChild("createdAt").startAt(start).endAt(end));
        }

        function addOrder(order) {

            //Format before save 
            _.forEach(order.services, function(service) {
                delete service.$id;
                delete service.$$hashKey;
                delete service.$priority;
            });

            var deffered = $q.defer();

            //Create Order Id
            var newOrderKey = firebaseDataService.orders.push().key;

            var update = {};

            update['orders/' + newOrderKey] = order;
            update['cars/' + order.car.id + '/orders/' + newOrderKey] = true;

            var total = order.amount.total;

            firebaseDataService.root.update(update, function(error) {
                if (error) {
                    deffered.reject(error)
                } else {
                    deffered.resolve(newOrderKey);
                }
            });

            return deffered.promise;
        }

        function removeOrder(order) {

            var deffered = $q.defer();

            var update = {};

            update['orders/' + order.$id] = null;
            update['cars/' + order.car.id + '/orders/' + order.$id] = null;

            var total = order.amount.total;

            firebaseDataService.root.update(update, function(error) {
                if (error) {
                    deffered.reject(error)
                } else {
                    deffered.resolve();
                }
            });

            return deffered.promise;
        }


    }
})();
