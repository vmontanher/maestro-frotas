(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('UserService', UserService);

    /** @ngInject */
    function UserService($firebaseObject, $firebaseArray, firebaseDataService) {

        var service = {
            getUser: getUser,
            getUsers: getUsers,
            getUsersCount: getUsersCount,
            getLastUsers: getLastUsers,
            getUserName: getUserName
        };

        return service;

        ////////////

        function getUser(userId) {
            return $firebaseObject(firebaseDataService.users.child(userId));
        }

        function getUsers() {
            return $firebaseArray(firebaseDataService.users);
        }

        function getUsersCount() {
            return $firebaseObject(firebaseDataService.users.child('count'));
        }

        function getLastUsers() {
            return $firebaseArray(firebaseDataService.users.orderByChild('createdAt').limitToLast(7));
        }

        function getUserName(userId) {
            return $firebaseObject(firebaseDataService.users.child(userId).child('fullname'));
        }

    }
})();
