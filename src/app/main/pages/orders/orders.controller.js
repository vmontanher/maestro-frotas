(function() {
    'use strict';

    angular
        .module('app.pages.orders')
        .controller('OrdersController', OrdersController);

    /** @ngInject */
    function OrdersController($state, $mdDialog, Orders, Toast, OrderService, DataTablePTBR) {
        var vm = this;

        // Data
        vm.orders = Orders;
        vm.dtInstance = {};

        //Methods
        vm.goToOrder = goToOrder;
        vm.deleteOrder = deleteOrder;

        init();

        //////////////

        function init() {
            loadOrdersTable();
        }

        function loadOrdersTable() {

            vm.dtOptions = {
                dom: 'rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
                columnDefs: [{
                    targets: [0, 1, 5],
                    responsivePriority: 1
                }, {
                    targets: 3,
                    render: function(data, type) {
                        if (type === 'display') {
                            var orderStatus = getOrderStatus(JSON.parse(data));
                            return '<span class="status ' + orderStatus.color + '">' + orderStatus.name + '</span>';
                        }

                        if (type === 'filter') {
                            return getOrderStatus(JSON.parse(data)).name;
                        }

                        return data;
                    }
                }, {
                    targets: 4,
                    type: 'date'
                }, {
                    // Target the actions column
                    targets: 6,
                    filterable: false,
                    sortable: false
                }],
                initComplete: function() {
                    var api = this.api(),
                        searchBox = angular.element('body').find('#orders-search');

                    // Bind an external input as a table wide search box
                    if (searchBox.length > 0) {
                        searchBox.on('keyup', function(event) {
                            api.search(event.target.value).draw();
                        });
                    }
                },
                pagingType: 'simple',
                order: [5, 'desc'],
                lengthMenu: [10, 20, 30, 50, 100],
                pageLength: 20,
                responsive: true,
                language: DataTablePTBR,
                scrollY: 'auto'
            };
        }

        function goToOrder(id) {
            $state.go('app.orders.detail', { id: id });
        }

        function getOrderStatus(statuses) {
            return _.first(statuses);
        }

        function deleteOrder(order, ev) {
            var confirm = $mdDialog.confirm()
                .title('Tem certeza que deseja remover esse agendamento?')
                .textContent('Essa ação não pode ser revertida.')
                .ariaLabel('remove order')
                .targetEvent(ev)
                .ok('Remover')
                .cancel('Cancelar');

            $mdDialog.show(confirm).then(function() {
                OrderService.removeOrder(order).then(function(error) {
                    if (error) {
                        Toast.show('Erro ao remover agendamento');
                    } else {
                        Toast.show('Agendamento removido');
                    }
                });
            });
        }
    }

})();
