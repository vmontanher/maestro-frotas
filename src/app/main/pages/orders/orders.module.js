 (function() {
     'use strict';

     angular
         .module('app.pages.orders', [])
         .config(config);

     /** @ngInject */
     function config($stateProvider, msNavigationServiceProvider) {
         // State
         $stateProvider
             .state('app.orders', {
                 url: '/orders',
                 views: {
                     'content@app': {
                         templateUrl: 'app/main/pages/orders/orders.html',
                         controller: 'OrdersController as vm',
                         resolve: {
                             User: function(AuthService) {
                                 return AuthService.firebaseAuthObject.$requireSignIn();
                             },
                             Orders: function(OrderService) {
                                 return OrderService.getOrders().$loaded();
                             }

                         }
                     }
                 }
             });

         msNavigationServiceProvider.saveItem('orders', {
             title: 'Agendamentos',
             icon: 'icon-calendar-clock',
             state: 'app.orders',
             weight: 3
         });


     }

 })();
