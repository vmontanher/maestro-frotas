(function() {
    'use strict';

    angular
        .module('app.pages.book')
        .controller('BookController', BookController);

    /** @ngInject */
    function BookController($state, $scope, $document, $mdDialog, $rootScope, $filter, Statuses, Quote, Services, AddressService, NotificationService, CarService, QuoteService, OrderService, ENV) {
        var vm = this;

        // Data
        vm.cars = [];
        vm.book = {};
        vm.services = Services;

        vm.serviceRequired = null;

        // Methods
        vm.resetBook = resetBook;

        vm.findCarByPlate = findCarByPlate;
        vm.openAddCarDialog = openAddCarDialog;

        vm.toogleService = toogleService;
        vm.existsService = existsService;
        vm.addOtherService = addOtherService;
        vm.addDiagnostic = addDiagnostic;

        vm.findAddressByZip = findAddressByZip;
        vm.setDate = setDate;

        vm.calculateQuote = calculateQuote;

        vm.submit = submit;

        init();

        //////////

        function init() {

            if (Quote && isQuoteCompleted(Quote)) {
                vm.book = angular.copy(Quote);
                vm.plate = vm.book.car.plate;
                vm.needQuote = false;
                vm.serviceRequired = true;
                vm.isQuoteCompleted = true;
            } else {
                resetBook();
            }

            //Schedule
            vm.availableTimes = [];

            //Times
            for (var i = 9; i <= 18; i++) {
                if (i < 10) {
                    vm.availableTimes.push('0' + i + ':00');
                    vm.availableTimes.push('0' + i + ':30');
                } else {
                    vm.availableTimes.push(i + ':00');
                    if (i !== 18)
                        vm.availableTimes.push(i + ':30');
                }
            }

            vm.minDate = moment(_.now()).add(36, 'hours').toDate();

            $rootScope.book = vm.book;
        }

        function resetBook() {
            vm.book.services = [];
            vm.book.amount = {
                total: 0
            };
            vm.needQuote = null;
            vm.serviceRequired = null;
        }

        function submit(ev) {
            if (vm.needQuote) {
                showConfirmQuote();
            } else {
                showConfirmOrder();
            }
        }

        // STEP Cars
        function findCarByPlate() {
            if (vm.plate) {
                vm.loadingCar = true;
                vm.carNotFound = false;
                CarService.getCarByPlate(vm.plate).$loaded(function(data) {
                    if (data[0]) {
                        CarService.getCar(data[0].$id).$loaded(function(data) {
                            vm.book.car = data;
                        });
                    } else {
                        vm.carNotFound = true;
                        delete vm.book.car;
                    }
                    resetBook();
                    vm.loadingCar = false;
                });
            } else {
                delete vm.book.car;
            }
        }

        function openAddCarDialog(ev) {
            $mdDialog.show({
                controller: 'AddCarDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/main/dialogs/add-car/add-car-dialog.html',
                parent: angular.element($document.find('#content-container')),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: true,
                locals: {
                    Book: vm.book
                },
                resolve: {
                    Makes: function(CarService) {
                        return CarService.getMakes().$loaded();
                    }
                },
                onRemoving: function(element) {
                    vm.plate = vm.book.car.plate;
                    vm.carNotFound = false;
                    resetBook();
                }
            });
        }

        // STEP Services
        function toogleService(service) {

            var index = _.findIndex(vm.book.services, {
                'id': service.$id
            });

            if (index > -1) {
                if (service.$id == "diagnostico-problema")
                    vm.book.problems = null;
                vm.book.services.splice(index, 1);
            } else {
                service.id = service.$id;
                vm.book.services.push(service);
            }

            if (_.isEmpty(vm.book.services)) {
                vm.serviceRequired = null;
            } else {
                vm.serviceRequired = false;
            }

            calculateQuote();
        }

        function existsService(service) {

            var index = _.findIndex(vm.book.services, {
                'id': service.$id
            });
            if (index > -1) {
                return true;
            }
            return false;
        }

        function addOtherService(ev) {

            var index = _.findIndex(vm.book.services, {
                'id': 'other-service'
            });

            var initialValue = null;

            if (index > -1)
                initialValue = vm.book.services[index].name;

            var otherService = $mdDialog.prompt()
                .title('O que você precisa?')
                .htmlContent('Mande para a gente tudo o que você precisa. Nós fazemos a cotação para você. <br>Ah! e não se preocupe, a maioria dos serviços podem ser feitos na garagem da sua casa ou escritório.')
                .ariaLabel('Other Service')
                .initialValue(initialValue)
                .targetEvent(ev)
                .ok('OK')
                .cancel('Cancelar');

            $mdDialog.show(otherService).then(function(result) {

                var service = {
                    id: 'other-service',
                    name: result
                }

                var index = _.findIndex(vm.book.services, {
                    'id': 'other-service'
                });

                if (!_.isEmpty(result)) {

                    if (index > -1) {
                        vm.book.services[index] = service;
                    } else {
                        vm.book.services.push(service);
                    }

                    vm.serviceRequired = false;

                } else {
                    if (index > -1)
                        vm.book.services.splice(index, 1);

                }
                calculateQuote();

            }, function() {});
        }

        function addDiagnostic(ev) {
            var diagnostic = $mdDialog.prompt()
                .title('O que está acontecendo?')
                .textContent('Descreva os problemas que estão ocorrendo com o seu carro.')
                .ariaLabel('Diagnostic')
                .initialValue(vm.book.problems)
                .targetEvent(ev)
                .ok('OK')
                .cancel('Cancelar');

            $mdDialog.show(diagnostic).then(function(result) {
                if (!_.isEmpty(result)) {
                    var service = vm.services.$getRecord("diagnostico-problema");
                    if (!existsService(service))
                        toogleService(service);
                    vm.book.problems = result;
                }

            }, function() {});
        }

        // STEP address
        function findAddressByZip() {
            vm.zipError = null;

            var address = vm.book.address;

            address.format = null;

            //Keep address number and complement
            var number = address.number ? address.number : '';
            var complement = address.complement ? address.complement : '';

            if (address.zip && address.zip.length == 8) {

                AddressService.getAddressByZip(address.zip)
                    .then(function(data) {

                        //ZipCode invalid
                        if (data.erro) {

                            //set error
                            vm.zipError = "CEP inválido";

                            vm.book.address = {
                                zip: address.zip,
                                number: number,
                                complement: complement
                            };

                            return;
                        }

                        //Set address by zipcode
                        vm.book.address = {
                            city: data.localidade,
                            zip: address.zip,
                            district: data.bairro,
                            number: number,
                            state: data.uf,
                            street: data.logradouro,
                            complement: complement
                        }

                    });
            } else {

                vm.book.address = {
                    number: number,
                    complement: complement

                };
            }
        }

        // STEP Schedule
        function setDate() {
            var hours = _.split(vm.time, ':')[0];
            var minutes = _.split(vm.time, ':')[1];

            var newDate;
            if (hours && minutes) {
                newDate = moment(vm.appointmentDate).hour(hours).minute(minutes).second(0).toDate();
            } else {
                newDate = moment(vm.appointmentDate).toDate();
            }

            vm.appointmentDate = newDate;
            vm.book.appointmentDate = vm.appointmentDate.getTime();
        }

        // Quote
        function calculateQuote() {

            vm.loadingPrice = true;

            vm.needQuote = false;

            var laborTime = 0;
            var amount = {
                parts: 0
            };

            var modelId = vm.book.car.model.id;

            CarService.getServicesByModel(modelId)
                .$loaded(function(loadedServices) {

                    if (_.isEmpty(loadedServices)) {
                        vm.needQuote = true;
                    } else {

                        _.forEach(vm.book.services, function(service, index) {
                            if (!service.id || service.id === 'other-service') {
                                vm.needQuote = true;
                            }

                            var price = _.find(loadedServices, {
                                '$id': service.id
                            });

                            if (service.hasParts && price) {
                                vm.book.services[index] = _.merge(service, price);
                                _.forEach(vm.book.services[index].parts, function(part) {
                                    amount.parts += (part.price * part.quantity);
                                });
                                laborTime += service.laborTime;
                            } else {
                                vm.needQuote = true;
                            }

                        });
                    }

                    if (laborTime > 0 && !vm.needQuote) {
                        vm.book.laborTime = laborTime;
                        amount.labor = _.round(((laborTime / 60) * 79.8), 2);
                        amount.total = _.round(amount.labor + amount.parts, 2);
                        vm.book.amount = amount;
                    } else {
                        vm.book.amount = {
                            total: 0
                        };
                        vm.book.laborTime = 0;
                    }

                    vm.loadingPrice = false;

                });
        }

        function saveQuote(ev) {

            var quote = createQuote(vm.book);

            QuoteService.addQuote(quote).then(function(refKey) {

                // Notify us about quote (Slack)
                if (ENV !== 'DEV') {
                    quote.id = refKey;
                    NotificationService.notifyQuote(quote);
                }

                showDialogQuoteSuccess(ev);
                $state.go('app.quotes');

            });
        }

        function createQuote(book) {
            var quote = {

                //Address
                address: book.address,

                status: [],

                car: {
                    //Id
                    id: book.car.$id,
                    //Make
                    make: {
                        id: book.car.make.id,
                        name: book.car.make.name
                    },
                    //Model
                    model: {
                        id: book.car.model.id,
                        name: book.car.model.name
                    },
                    //Year
                    year: book.car.year,
                    //Plate
                    plate: book.car.plate
                },

                //Services
                services: book.services,

                createdAt: _.now(),

                problems: book.problems || null

            };

            var status = _.find(Statuses, {
                '$id': 'quote-pending'
            });

            quote.status.push({
                id: status.$id,
                name: status.name,
                color: status.color,
                date: _.now()
            });

            return quote;
        }

        function isQuoteCompleted(quote) {
            return _.first(quote.status).id === 'quote-completed';
        }

        function showDialogQuoteSuccess(ev) {
            $mdDialog.show(
                $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Orçamento solicitado')
                .htmlContent('Recebemos sua solicitação de orçamento! Entraremos em contato informando mais detalhes da sua solicitação. <br> Caso tenha alguma dúvida não exite em nos contatar pelos nossos canais no menu "Ajuda".')
                .ariaLabel('Quote Successs')
                .ok('Ok')
                .targetEvent(ev)
            );
        }

        function showConfirmQuote(ev) {

            var html = "<p>Ao clicar em 'Solicitar Orçamento' seu pedido de orçamento será realizado.</p>"
            html += '<p><b>Carro:</b> ' + $filter('car')(vm.book.car) + '</p>';
            html += '<p><b>Serviços:</b> ' + $filter('services')(vm.book.services) + '</p>';
            html += '<p><b>Endereço:</b> ' + $filter('address')(vm.book.address) + '</p>';

            var confirm = $mdDialog.confirm()
                .title('Confirmação de orçamento')
                .htmlContent(html)
                .ariaLabel('confirm quote')
                .targetEvent(ev)
                .ok('Solicitar Orçamento')
                .cancel('Cancelar');

            $mdDialog.show(confirm).then(function() {
                saveQuote(ev);
            });
        }

        // Order
        function saveOrder(ev) {

            var order = createOrder(vm.book);

            OrderService.addOrder(order).then(function(refKey) {

                if (ENV !== 'DEV') {
                    order.id = refKey;
                    NotificationService.notifyOrder(order);
                }

                if (vm.isQuoteCompleted) {
                    var status = _.find(Statuses, {
                        '$id': 'appointment-booked'
                    });
                    Quote.status.unshift({
                        id: status.$id,
                        name: status.name,
                        color: status.color,
                        date: _.now()
                    });
                    Quote.$save();
                }

                showDialogOrderSuccess(ev);
                $state.go('app.orders');

            });
        }

        function createOrder(book) {
            var order = {

                status: book.status || [],

                problems: book.problems || null,

                //Car
                car: {
                    //Id
                    id: book.car.$id || book.car.id,
                    //Make
                    make: {
                        id: book.car.make.id,
                        name: book.car.make.name
                    },
                    //Model
                    model: {
                        id: book.car.model.id,
                        name: book.car.model.name
                    },
                    //Year
                    year: book.car.year,
                    //Plate
                    plate: book.car.plate
                },

                //Address
                address: book.address,

                //Services
                services: book.services,

                //Appointment Date
                appointmentDate: book.appointmentDate,

                //Labor Time
                laborTime: book.laborTime,

                //Prices
                amount: book.amount,

                createdAt: _.now()

            };

            var status = _.find(Statuses, {
                '$id': 'appointment-booked'
            });

            order.status.unshift({
                id: status.$id,
                name: status.name,
                color: status.color,
                date: _.now()
            });

            return order;
        }

        function showConfirmOrder(ev) {

            var html = "<p>Ao clicar em 'Agendar' seu agendamento será realizado.</p>"
            html += '<p><b>Carro:</b> ' + $filter('car')(vm.book.car) + '</p>';
            html += '<p><b>Serviços:</b> ' + $filter('services')(vm.book.services) + '</p>';
            html += '<p><b>Endereço:</b> ' + $filter('address')(vm.book.address) + '</p>';
            html += '<p><b>Agendado para:</b> ' + moment(vm.book.appointmentDate).format("LLLL") + '</p>';
            html += '<p><b>Total:</b> ' + $filter('currency')(vm.book.amount.total) + '</p>';

            var confirm = $mdDialog.confirm()
                .title('Confirmação de agendamento')
                .htmlContent(html)
                .ariaLabel('confirm appointment')
                .targetEvent(ev)
                .ok('Agendar')
                .cancel('Cancelar');

            $mdDialog.show(confirm).then(function() {
                saveOrder(ev);
            });
        }

        function showDialogOrderSuccess(ev) {
            $mdDialog.show(
                $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Agendamento realizado')
                .htmlContent('Seu agendamento foi realizado! Para confirmar seu agendamento entre com seus dados de pagamento.<br>Caso tenha alguma dúvida não exite em nos contatar pelos nossos canais no menu "Ajuda".')
                .ariaLabel('Appointment Success')
                .ok('Ok')
                .targetEvent(ev)
            );
        }

    }

})();
