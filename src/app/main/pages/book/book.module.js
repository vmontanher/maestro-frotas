(function() {
    'use strict';

    angular
        .module('app.pages.book', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider, $translatePartialLoaderProvider) {
        // State
        $stateProvider
            .state('app.book', {
                url: '/book?quoteId',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/pages/book/book.html',
                        controller: 'BookController as vm',
                        resolve: {
                            User: function(AuthService) {
                                return AuthService.firebaseAuthObject.$requireSignIn();
                            },
                            Statuses: function(StatusesService) {
                                return StatusesService.getStatuses().$loaded();
                            },  
                            Services: function($q, CarService, QuoteService) {
                                return CarService.getAllServices().$loaded();
                            },
                            Quote: function(QuoteService, $stateParams) {
                                if($stateParams.quoteId)
                                    return QuoteService.getQuote($stateParams.quoteId);
                                return null;
                            } 
                        }
                    }
                }
            })

        msNavigationServiceProvider.saveItem('book', {
            title: 'Novo Pedido',
            icon: 'icon-account',
            state: 'app.book',
            weight: 0
        });

        //Translation
        $translatePartialLoaderProvider.addPart('app/main/pages/book');

    }
})();
