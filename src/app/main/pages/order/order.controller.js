(function() {
    'use strict';

    angular
        .module('app.pages.order')
        .controller('OrderController', OrderController);

    /** @ngInject */
    function OrderController($state, $rootScope, User, Order, Statuses, Services, Toast, CarService, RoleService) {
        var vm = this;

        // Data
        vm.order = Order;
        vm.statuses = Statuses;
        vm.services = [];
        // Methods

        vm.save = save;

        vm.calculatePriceByService = calculatePriceByService;
        vm.searchService = searchService;
        vm.transformChipService = transformChipService;
        vm.addPart = addPart;
        vm.removePart = removePart;

        vm.isAppointmentBooked = isAppointmentBooked
        vm.isMechanicOnTheWay = isMechanicOnTheWay;
        vm.isAppointmentCompleted = isAppointmentCompleted;
        vm.hasStatus = hasStatus;
        vm.updateStatus = updateStatus;

        init();

        //////////

        function init() {

            CarService.getServicesByModel(vm.order.car.model.id)
                .$loaded(function(loadedServices) {

                    _.forEach(Services, function(service) {
                        var price = _.find(loadedServices, {
                            '$id': service.$id
                        });
                        vm.services.push(_.merge(service, price));
                    });
                });

            RoleService.isMaster(User.uid).$loaded(function(snapshot) {
                vm.hasPermission = snapshot.$value || false;
            });

            $rootScope.$on('logout', function() {
                vm.order.$destroy();
            });
        }

        function calculatePriceByService(service) {
            if (service.hasParts) {
                var parts = 0;
                var labor = _.round(((service.laborTime / 60) * 79.8), 2);
                _.forEach(service.parts, function(part) {
                    parts += (part.price * part.quantity);
                });
                return labor + parts;
            } else if (service.laborTime) {
                return _.round(((service.laborTime / 60) * 79.8), 2);
            }
        }

        function save() {
            var laborTime, parts, total;
            laborTime = parts = total = 0;

            _.forEach(vm.order.services, function(service) {
                laborTime += service.laborTime;
                if (service.hasParts) {
                    _.forEach(service.parts, function(part, index) {
                        if (!part.price || !part.quantity) {
                            service.parts.splice(index, 1);
                        } else {
                            parts += (part.price * part.quantity);
                        }
                    });
                }
            });
            vm.order.laborTime = laborTime;
            var labor = _.round(((laborTime / 60) * 79.8), 2);
            vm.order.amount = {
                labor: labor,
                parts: parts,
                total: labor + parts
            };

            vm.order.$save().then(function() {
                Toast.show('Agendamento salvo');
            })
        }

        //SERVICE
        function searchService(query) {
            var results = query ? vm.services.filter(createFilterFor(query)) : [];
            return results;
        }

        function createFilterFor(query) {
            var lowercaseQuery = _.lowerCase(query);
            return function filterFn(service) {
                return _.lowerCase(service.name).indexOf(lowercaseQuery) > -1;
            };
        }

        function transformChipService(chip) {

            if (angular.isObject(chip)) {
                chip.id = chip.$id;

                var service = _.find(vm.order.services, function(o) {
                    return o.id == chip.id;
                });

                return service || chip;
            }
            return { name: chip }
        }

        function addPart() {
            if (!vm.editService.parts)
                vm.editService.parts = [];

            vm.editService.parts.push({
                name: null,
                price: null,
                quantity: null
            });
        }

        function removePart($index) {
            vm.editService.parts.splice($index, 1);
        }

        //STATUS
        function isAppointmentBooked() {
            return _.first(vm.order.status).id == 'appointment-booked';
        }

        function isMechanicOnTheWay() {
            return _.first(vm.order.status).id == 'mechanic-on-the-way';
        }

        function isAppointmentCompleted() {
            return _.first(vm.order.status).id == 'appointment-completed';
        }

        function hasStatus(status) {
            return _.some(vm.order.status, { id: status });
        }

        function updateStatus(id) {
            if (!id) {
                return;
            }
            var status = _.find(vm.statuses, function(o) {
                return o.$id == id;
            });

            vm.order.status.unshift({
                id: status.$id,
                name: status.name,
                color: status.color,
                date: _.now()
            });

            vm.newStatus = '';
        }


    }

})();
