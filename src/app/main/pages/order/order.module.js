(function() {
    'use strict';

    angular
        .module('app.pages.order', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider) {

        // State
        $stateProvider.state('app.orders.detail', {
            url: '/:id',
            views: {
                'content@app': {
                    templateUrl: 'app/main/pages/order/order.html',
                    controller: 'OrderController as vm',
                    resolve: {
                        User: function(AuthService) {
                            return AuthService.firebaseAuthObject.$requireSignIn();
                        },
                        Order: function($stateParams, OrderService) {
                            return OrderService.getOrder($stateParams.id).$loaded();
                        },
                        Statuses: function(StatusesService) {
                            return StatusesService.getStatuses().$loaded();
                        },
                        Services: function(CarService) {
                            return CarService.getAllServices().$loaded();
                        }
                    }
                }
            }
        });



    }

})();
