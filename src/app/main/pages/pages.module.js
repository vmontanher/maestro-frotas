(function() {
    'use strict';

    angular.module('app.pages', [
        'app.pages.login',
        'app.pages.forgot-password',
        'app.pages.reset-password',
        'app.pages.home',
        'app.pages.quote',
        'app.pages.quotes',
        'app.pages.car',
        'app.pages.cars',
        'app.pages.order',
        'app.pages.orders',
        'app.pages.book'
    ]);


})();