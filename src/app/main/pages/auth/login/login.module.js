(function() {
    'use strict';

    angular
        .module('app.pages.login', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider) {
        // State
        $stateProvider.state('app.login', {
            url: '/login',
            views: {
                'main@': {
                    templateUrl: 'app/core/layouts/content-only.html',
                    controller: 'MainController as vm'
                },
                'content@app.login': {
                    templateUrl: 'app/main/pages/auth/login/login.html',
                    controller: 'LoginController as vm',
                    resolve: {
                        currentUser: function(AuthService) {
                            return AuthService.firebaseAuthObject.$waitForSignIn();
                        }
                    }
                }

            },
            bodyClass: 'login'
        });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/pages/auth/login');


    }

})();