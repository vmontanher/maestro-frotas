(function() {
    'use strict';

    angular
        .module('app.pages.login')
        .controller('LoginController', LoginController);

    /** @ngInject */
    function LoginController($state, AuthService, UserService, StorageService, $animate) {

        var vm = this;

        // Data

        // Methods
        vm.login = login;

        init()

        //////////

        function init() {
            if (AuthService.isLoggedIn()) {
                $state.go('app.home');
            }
        }

        function login() {
            vm.loading = true;

            AuthService.login(vm.user)
                .then(function(authData) {
                    var profile = UserService.getUser(authData.uid);

                    profile.$loaded(function(data) {
                        profile.lastLogin = _.now();
                        profile.createdAt = _.now();
                        $state.go('app.home');
                    });
                }).catch(function(error) {
                    switch (error.code) {
                        case 'auth/user-not-found':
                            vm.error = 'LOGIN.ERRORS.EMAIL_OR_PASSWORD_INVALID';
                            break;
                        case 'auth/wrong-password':
                            vm.error = 'LOGIN.ERRORS.EMAIL_OR_PASSWORD_INVALID';
                            break;
                        case 'auth/invalid-email':
                            vm.error = 'LOGIN.ERRORS.EMAIL_MUST_VALID';
                            break;
                    }
                    vm.user.password = null;
                    vm.loading = false;
                })
        }
    }
})();
