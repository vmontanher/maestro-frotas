(function() {
    'use strict';

    angular
        .module('app.pages.forgot-password')
        .controller('ForgotPasswordController', ForgotPasswordController);

    /** @ngInject */
    function ForgotPasswordController(AuthService, $mdDialog) {
        var vm = this;

        // Data

        // Methods
        vm.sendPasswordResetEmail = sendPasswordResetEmail;
        //////////

        function sendPasswordResetEmail() {
            AuthService.firebaseAuthObject.$sendPasswordResetEmail(vm.email).then(function() {
                vm.sended = true;
            }).catch(function(error) {
                vm.sended = true;
            });
        }
    }
})();