(function() {
    'use strict';

    angular
        .module('app.pages.quote', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider) {

        // State
        $stateProvider.state('app.quotes.detail', {
            url: '/:id',
            views: {
                'content@app': {
                    templateUrl: 'app/main/pages/quote/quote.html',
                    controller: 'QuoteController as vm',
                    resolve: {
                        User: function(AuthService) {
                            return AuthService.firebaseAuthObject.$requireSignIn();
                        },
                        Quote: function($stateParams, QuoteService) {
                            return QuoteService.getQuote($stateParams.id).$loaded();
                        },
                        Statuses: function(StatusesService) {
                            return StatusesService.getStatuses().$loaded();
                        },
                        Services: function(CarService) {
                            return CarService.getAllServices().$loaded();
                        }
                    }
                }
            }
        });



    }

})();
