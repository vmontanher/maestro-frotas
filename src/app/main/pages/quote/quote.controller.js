(function() {
    'use strict';

    angular
        .module('app.pages.quote')
        .controller('QuoteController', QuoteController);

    /** @ngInject */
    function QuoteController($state, $rootScope, $mdDialog, $filter, User, Quote, Statuses, Services, Toast, QuoteService, CarService, RoleService) {
        var vm = this;

        // Data
        vm.quote = Quote;
        vm.statuses = Statuses;
        vm.services = [];
        // Methods

        vm.save = save;

        vm.calculatePriceByService = calculatePriceByService;
        vm.searchService = searchService;
        vm.transformChipService = transformChipService;
        vm.addPart = addPart;
        vm.removePart = removePart;

        vm.isQuoteCompleted = isQuoteCompleted;
        vm.isQuotePending = isQuotePending;
        vm.isAppointmentBooked = isAppointmentBooked;
        vm.updateStatus = updateStatus;
        vm.hasStatus = hasStatus;

        vm.saveServiceAsDefault = saveServiceAsDefault;

        init();

        //////////

        function init() {

            CarService.getServicesByModel(vm.quote.car.model.id)
                .$loaded(function(loadedServices) {

                    _.forEach(Services, function(service) {
                        var price = _.find(loadedServices, {
                            '$id': service.$id
                        });
                        vm.services.push(_.merge(service, price));
                    });
                });

            RoleService.isMaster(User.uid).$loaded().then(function(snapshot) {
                vm.hasPermission = snapshot.$value ||  false;
            });

            $rootScope.$on('logout', function() {
                vm.quote.$destroy();
            });
        }

        function calculatePriceByService(service) {
            if (service.hasParts) {
                var parts = 0;
                var labor = _.round(((service.laborTime / 60) * 79.8), 2);
                _.forEach(service.parts, function(part) {
                    parts += (part.price * part.quantity);
                });
                return labor + parts;
            } else if (service.laborTime) {
                return _.round(((service.laborTime / 60) * 79.8), 2);
            }
        }

        function save() {
            var laborTime, parts, total;
            laborTime = parts = total = 0;

            _.forEach(vm.quote.services, function(service) {
                laborTime += service.laborTime;
                if (service.hasParts) {
                    _.forEach(service.parts, function(part, index) {
                        if (!part.price || !part.quantity) {
                            service.parts.splice(index, 1);
                        } else {
                            parts += (part.price * part.quantity);
                        }
                    });
                }
            });

            vm.quote.laborTime = laborTime;

            var labor = _.round(((laborTime / 60) * 79.8), 2);
            vm.quote.amount = {
                labor: labor,
                parts: parts,
                total: labor + parts
            };

            vm.quote.$save().then(function() {
                Toast.show('Orçamento salvo');
            })
        }

        function removeQuote(ev) {
            var confirm = $mdDialog.confirm()
                .title('Tem certeza que deseja remover esse orçamento?')
                .ariaLabel('Remove quote')
                .targetEvent(ev)
                .ok('Remover')
                .cancel('Cancelar');

            $mdDialog.show(confirm).then(function() {
                QuoteService.removeQuote(vm.quote).then(function() {
                    Toast.show('Orçamento removido');
                    $state.go('app.quotes');
                }, function(error) {
                    Toast.show('Erro ao remover orçamento');
                });

            });
        }

        //Services
        function searchService(query) {
            var results = query ? vm.services.filter(createFilterFor(query)) : [];
            return results;
        }

        function createFilterFor(query) {
            var lowercaseQuery = _.lowerCase(query);
            return function filterFn(service) {
                return _.lowerCase(service.name).indexOf(lowercaseQuery) > -1;
            };
        }

        function transformChipService(chip) {

            if (angular.isObject(chip)) {
                chip.id = chip.$id;

                var service = _.find(vm.quote.services, function(o) {
                    return o.id == chip.id;
                });

                return service || chip;
            }
            return { name: chip }
        }

        function addPart() {
            if (!vm.editService.parts)
                vm.editService.parts = [];

            vm.editService.parts.push({
                name: null,
                price: null,
                quantity: null
            });
        }

        function removePart($index) {
            vm.editService.parts.splice($index, 1);
        }

        //Status
        function isQuotePending() {
            return _.first(vm.quote.status).id == 'quote-pending';
        }

        function isQuoteCompleted() {
            return _.first(vm.quote.status).id == 'quote-completed';
        }

        function isAppointmentBooked() {
            return _.first(vm.quote.status).id == 'appointment-booked';
        }

        function hasStatus(status) {
            return _.some(vm.quote.status, { id: status });
        }

        function updateStatus(id) {
            if (!id) {
                return;
            }
            var status = _.find(vm.statuses, function(o) {
                return o.$id == id;
            });

            vm.quote.status.unshift({
                id: status.$id,
                name: status.name,
                color: status.color,
                date: _.now()
            });

            vm.newStatus = '';
        }

        function saveServiceAsDefault() {
            if (!vm.editService.laborTime && !vm.edit.parts) {
                return alert("Preencha os campos Tempo e Peças");
            }

            CarService.addService(vm.quote.car, vm.editService).then(function() {
                Toast.show('Serviço salvo para ' + vm.quote.car.make.name + ' ' + vm.quote.car.model.name);
            })
        }
    }

})();
