(function() {
    'use strict';

    angular
        .module('app.pages.cars')
        .controller('CarsController', CarsController);

    /** @ngInject */
    function CarsController($state, $mdDialog, $document, Cars, Toast, CarService, DataTablePTBR) {
        var vm = this;

        // Data
        vm.cars = Cars;
        vm.dtInstance = {};

        //Methods
        vm.goToCar = goToCar;
        vm.openAddCarDialog = openAddCarDialog;

        init();

        //////////////

        function init() {
            loadCarsTable();
        }

        function loadCarsTable() {

            vm.dtOptions = {
                dom: 'rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
                columnDefs: [{
                    targets: [0, 5],
                    responsivePriority: 0
                }, {
                    targets: 0,
                    sortable: false
                }, {
                    // Target the image column
                    targets: 1,
                    filterable: false,
                    sortable: false,
                    width: '80px'
                }, {
                    // Target the actions column
                    targets: 5,
                    filterable: false,
                    sortable: false
                }],
                initComplete: function() {
                    var api = this.api(),
                        searchBox = angular.element('body').find('#cars-search');

                    // Bind an external input as a table wide search box
                    if (searchBox.length > 0) {
                        searchBox.on('keyup', function(event) {
                            api.search(event.target.value).draw();
                        });
                    }
                },
                pagingType: 'simple',
                order: [0, 'asc'],
                lengthMenu: [10, 20, 30, 50, 100],
                pageLength: 20,
                responsive: true,
                language: DataTablePTBR,
                scrollY: 'auto'
            };
        }

        function goToCar(id) {
            $state.go('app.cars.detail', { id: id });
        }

        function deleteCar(car, ev) {
            var confirm = $mdDialog.confirm()
                .title('Tem certeza que deseja remover esse carro?')
                .textContent('Essa ação não pode ser revertida.')
                .ariaLabel('remove car')
                .targetEvent(ev)
                .ok('Remover')
                .cancel('Cancelar');

            $mdDialog.show(confirm).then(function() {
                CarService.removeCar(car).then(function(error) {
                    if (error) {
                        Toast.show('Erro ao remover carro');
                    } else {
                        Toast.show('Carro removido');
                    }
                });
            });
        }

        function openAddCarDialog(ev) {
            $mdDialog.show({
                controller: 'AddCarDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/main/dialogs/add-car/add-car-dialog.html',
                parent: angular.element($document.find('#content-container')),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: true,
                locals: {
                    Book: null
                },
                resolve: {
                    Makes: function(CarService) {
                        return CarService.getMakes().$loaded();
                    }
                }
            });
        }
    }

})();
