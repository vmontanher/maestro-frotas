 (function() {
     'use strict';

     angular
         .module('app.pages.cars', [])
         .config(config);

     /** @ngInject */
     function config($stateProvider, msNavigationServiceProvider) {

         // State
         $stateProvider.state('app.cars', {
             url: '/cars',
             views: {
                 'content@app': {
                     templateUrl: 'app/main/pages/cars/cars.html',
                     controller: 'CarsController as vm',
                     resolve: {
                         User: function(AuthService) {
                             return AuthService.firebaseAuthObject.$requireSignIn();
                         },
                         Cars: function(CarService) {
                             return CarService.getCars().$loaded();
                         }
                     }
                 }
             }
         });

         msNavigationServiceProvider.saveItem('cars', {
             title: 'Carros',
             icon: 'icon-car',
             state: 'app.cars',
             weight: 3
         });


     }

 })();
