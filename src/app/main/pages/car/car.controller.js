(function() {
    'use strict';

    angular
        .module('app.pages.car')
        .controller('CarController', CarController);

    /** @ngInject */
    function CarController($state, $rootScope, $mdDialog, Car, Toast, QuoteService, OrderService, DataTablePTBR) {
        var vm = this;

        // Data
        vm.car = Car;
        vm.orders = [];
        vm.quotes = [];
        vm.dtQuotesInstance = {};
        vm.dtOrdersInstance = {};

        //Methods
        vm.goToQuote = goToQuote;
        vm.goToOrder = goToOrder;

        vm.isQuoteCompleted = isQuoteCompleted;

        init();

        //////////////

        function init() {
            loadQuotesTable();
            loadOrdersTable();

            for (var key in vm.car.quotes) {
                addQuote(key);
            }
            for (var key in vm.car.orders) {
                addOrder(key);
            }

        }

        function getStatus(statuses) {
            return _.first(statuses);
        }

        //QUOTES
        function loadQuotesTable() {

            vm.dtQuotesOptions = {
                dom: 'rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
                columnDefs: [{
                    targets: [0, 3],
                    responsivePriority: 1
                }, {
                    targets: 1,
                    sortable: true,
                    render: function(data, type) {
                        if (type === 'display') {
                            if (data) {
                                var quoteStatus = getStatus(JSON.parse(data));
                                return '<span class="status ' + quoteStatus.color + '">' + quoteStatus.name + '</span>';
                            }
                            return '<span class="status md-black-bg">Não definido</span>';
                        }

                        if (type === 'filter') {
                            return '';
                        }

                        return data;
                    }
                }, {
                    targets: 2,
                    type: 'date'
                }, {
                    // Target the actions column
                    targets: 3,
                    filterable: false,
                    sortable: false
                }],
                pagingType: 'simple',
                order: [1, 'desc'],
                lengthMenu: [10, 20, 30, 50, 100],
                pageLength: 20,
                responsive: true,
                language: DataTablePTBR,
            };
        }

        function addQuote(quoteId) {
            QuoteService.getQuote(quoteId).$loaded(function(quote) {
                vm.quotes.push(quote);
            });
        }

        function removeQuote(quoteId) {
            _.remove(vm.quotes, function(obj) {
                return obj.$id == quoteId;
            });
        }

        function goToQuote(id) {
            $state.go('app.quotes.detail', { id: id });
        }

        function deleteQuote(quote, ev) {
            var confirm = $mdDialog.confirm()
                .title('Tem certeza que deseja remover esse orçamento?')
                .textContent('Essa ação não pode ser revertida.')
                .ariaLabel('remove quote')
                .targetEvent(ev)
                .ok('Remover')
                .cancel('Cancelar');

            $mdDialog.show(confirm).then(function() {
                removeQuote(quote.$id);
                QuoteService.removeQuote(quote).then(function(refKey) {
                    Toast.show('Orçamento removido');
                }, function(error) {
                    Toast.show('Erro ao remover orçamento');
                });
            });
        }

        function isQuoteCompleted(quote) {
            return _.first(quote.status).id == 'quote-completed';
        }

        //ORDERS
        function loadOrdersTable() {

            vm.dtOrdersOptions = {
                dom: 'rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
                columnDefs: [{
                    targets: [0, 1, 4],
                    responsivePriority: 1
                }, {
                    targets: 1,
                    render: function(data, type) {
                        if (type === 'display') {
                            if (data) {
                                var orderStatus = getStatus(JSON.parse(data));
                                return '<span class="status ' + orderStatus.color + '">' + orderStatus.name + '</span>';
                            }
                            return '<span class="status md-500-red-bg">Problema</span>';
                        }

                        if (type === 'filter') {
                            return '';
                        }

                        return data;
                    }
                }, {
                    targets: 2,
                    type: 'date'
                }, {
                    // Target the actions column
                    targets: 4,
                    filterable: false,
                    sortable: false
                }],
                pagingType: 'simple',
                order: [2, 'desc'],
                lengthMenu: [10, 20, 30, 50, 100],
                pageLength: 20,
                responsive: true,
                language: DataTablePTBR
            };
        }

        function addOrder(orderId) {
            OrderService.getOrder(orderId).$loaded(function(order) {
                vm.orders.push(order);
            });
        }

        function removeOrder(orderId) {
            _.remove(vm.orders, function(obj) {
                return obj.$id == orderId;
            });
        }

        function goToOrder(id) {
            $state.go('app.orders.detail', { id: id });
        }

        function deleteOrder(order, ev) {
            var confirm = $mdDialog.confirm()
                .title('Tem certeza que deseja remover esse agendamento?')
                .textContent('Essa ação não pode ser revertida.')
                .ariaLabel('remove order')
                .targetEvent(ev)
                .ok('Remover')
                .cancel('Cancelar');

            $mdDialog.show(confirm).then(function() {
                removeOrder(order.$id);
                OrderService.removeOrder(order).then(function(error) {
                    if (error) {
                        Toast.show('Erro ao remover agendamento');
                    } else {
                        Toast.show('Agendamento removido');

                    }
                });
            });
        }

    }

})();
