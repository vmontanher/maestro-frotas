(function() {
    'use strict';

    angular
        .module('app.pages.car', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider) {

        // State
        $stateProvider.state('app.cars.detail', {
            url: '/:id',
            views: {
                'content@app': {
                    templateUrl: 'app/main/pages/car/car.html',
                    controller: 'CarController as vm',
                    resolve: {
                        User: function(AuthService) {
                            return AuthService.firebaseAuthObject.$requireSignIn();
                        },
                        Car: function($stateParams, CarService) {
                            return CarService.getCar($stateParams.id).$loaded();
                        }
                    }
                }
            }
        });
    }

})();
