(function() {
    'use strict';

    angular
        .module('app.pages.home')
        .controller('HomeController', HomeController);

    /** @ngInject */
    function HomeController($state, $rootScope, Quotes, OrdersTM, OrdersTW, OrdersLW, Analytics) {
        var vm = this;

        // Methods
        vm.analytics = Analytics;

        init();

        //////////

        function init() {

            vm.ordersCountTW = OrdersTW.length;
            vm.ordersCountTM = OrdersTM.length;
            vm.ordersTotalTW = vm.ordersTotalTM = 0;
            vm.quotesCountCompleted = vm.quotesCountPending = 0;

            _.forEach(OrdersTW, function(order) {
                if (isAppointmentCompleted(order)) {
                    vm.ordersTotalTW += order.amount.total;
                }
            });

            _.forEach(OrdersTM, function(order) {
                if (isAppointmentCompleted(order)) {
                    vm.ordersTotalTM += order.amount.total;
                }
            });
            // This Week
            var startTW = moment().startOf('isoWeek');

            var startLW = moment().startOf('isoWeek').subtract(7, 'days');
            var endLW = moment().endOf('isoWeek').subtract(7, 'days');

            vm.quotesTW = vm.quotesLW = [];

            _.forEach(Quotes, function(quote) {
                //This week
                if (moment(quote.createdAt).isBetween(startTW, _.now())) {
                    vm.quotesTW.push(quote);
                }
                //Last week
                else if (moment(quote.createdAt).isBetween(startLW, endLW)) {
                    vm.quotesLW.push(quote);
                }

                if (isQuoteCompleted(quote)) {
                    vm.quotesCountCompleted++;
                } else if (isQuotePending(quote)) {
                    vm.quotesCountPending++;
                }
            });

            initMainChart();
        }

        function initMainChart() {

            var mainChart = [{
                key: "Orçamentos",
                values: {}
            }, {
                key: "Agendamentos",
                values: {}
            }];

            var supporting = {
                quotes: {
                    label: "Orçamentos",
                    chart: {
                        key: "quantidade",
                        values: {}
                    }
                },
                orders: {
                    label: "Agendamentos",
                    chart: {
                        key: "quantidade",
                        values: {}
                    }
                }
            }

            mainChart[0].values['TW'] = createDataMainChart(vm.quotesTW);
            //supporting.quotes.chart.values['TW'] = createDataSupportingChart(vm.quotesTW);

            mainChart[0].values['LW'] = createDataMainChart(vm.quotesLW);
            //supporting.quotes.chart.values['LW'] = createDataSupportingChart(vm.quotesLW);

            mainChart[1].values['TW'] = createDataMainChart(OrdersTW);
            //supporting.orders.chart.values['TW'] = createDataSupportingChart(OrdersTW);

            mainChart[1].values['LW'] = createDataMainChart(OrdersLW);
            //supporting.orders.chart.values['LW'] = createDataSupportingChart(OrdersLW);

            initWidgetChart();


            function initWidgetChart() {
                vm.widget5 = {
                    title: "Últimos Pedidos",
                    mainChart: {
                        config: {
                            refreshDataOnly: true,
                            deepWatchData: true
                        },
                        options: {
                            chart: {
                                type: 'multiBarChart',
                                color: ['#03a9f4', '#7FFF00'],
                                height: 420,
                                margin: {
                                    top: 8,
                                    right: 16,
                                    bottom: 32,
                                    left: 32
                                },
                                clipEdge: true,
                                groupSpacing: 0.3,
                                reduceXTicks: false,
                                stacked: false,
                                duration: 250,
                                x: function(d) {
                                    return d.x;
                                },
                                y: function(d) {
                                    return d.y;
                                },
                                yAxis: {
                                    tickFormat: function(d) {
                                        return d;
                                    }
                                },
                                legend: {
                                    margin: {
                                        top: 8,
                                        bottom: 32
                                    }
                                },
                                controls: {
                                    margin: {
                                        top: 8,
                                        bottom: 32
                                    }
                                },
                                tooltip: {
                                    gravity: 's',
                                    classes: 'gravity-s'
                                }
                            }
                        },
                        data: []
                    },
                    supporting: {
                        widgets: {
                            quotes: {
                                data: supporting.quotes,
                                chart: {
                                    data: []
                                }
                            },
                            orders: {
                                data: supporting.orders,
                                chart: {
                                    data: []
                                }
                            }
                        },
                        chart: {
                            config: {
                                refreshDataOnly: true,
                                deepWatchData: true
                            },
                            options: {
                                chart: {
                                    type: 'lineChart',
                                    color: ['#03A9F4'],
                                    height: 50,
                                    margin: {
                                        top: 8,
                                        right: 0,
                                        bottom: 0,
                                        left: 0
                                    },
                                    isArea: true,
                                    interpolate: 'cardinal',
                                    clipEdge: true,
                                    duration: 500,
                                    showXAxis: false,
                                    showYAxis: false,
                                    showLegend: false,
                                    useInteractiveGuideline: true,
                                    x: function(d) {
                                        return d.x;
                                    },
                                    y: function(d) {
                                        return d.y;
                                    },
                                    yDomain: [0, 9],
                                    xAxis: {
                                        tickFormat: function(d) {
                                            return vm.widget5.days[d];
                                        }
                                    },
                                    interactiveLayer: {
                                        tooltip: {
                                            gravity: 'e',
                                            classes: 'gravity-e'
                                        }
                                    }
                                }
                            },
                            data: []
                        }
                    },
                    days: ['Seg', 'Ter', 'Quar', 'Qui', 'Sex', 'Sáb', 'Dom'],
                    ranges: { TW: "Essa semana", LW: "Semana Passada" },
                    currentRange: '',
                    changeRange: function(range) {
                        vm.widget5.currentRange = range;

                        angular.forEach(mainChart, function(chartData, index) {
                            vm.widget5.mainChart.data[index] = {
                                key: chartData.key,
                                values: chartData.values[range]
                            };
                        });

                        angular.forEach(supporting, function(widget, name) {
                            vm.widget5.supporting.widgets[name].chart.data[0] = {
                                key: widget.chart.key,
                                values: widget.chart.values[range]
                            };
                        });

                    },
                    init: function() {
                        vm.widget5.changeRange('TW');
                    }
                };

                vm.widget5.init();
            }
        }

        function createDataMainChart(data) {
            var mainChart = [
                { x: "Seg", y: 0 },
                { x: "Ter", y: 0 },
                { x: "Qua", y: 0 },
                { x: "Qui", y: 0 },
                { x: "Sex", y: 0 },
                { x: "Sáb", y: 0 },
                { x: "Dom", y: 0 }
            ];

            _.forEach(data, function(obj) {
                mainChart[moment(obj.createdAt).isoWeekday() - 1].y++;
            });

            return mainChart;
        }

        function createDataSupportingChart(data) {
            var supportingChart = [
                { x: 0, y: 0 },
                { x: 1, y: 0 },
                { x: 2, y: 0 },
                { x: 3, y: 0 },
                { x: 4, y: 0 },
                { x: 5, y: 0 },
                { x: 6, y: 0 }
            ];

            _.forEach(data, function(obj) {
                supportingChart[moment(obj.createdAt).isoWeekday() - 1].y++;
            });

            return supportingChart;
        }

        function isAppointmentCompleted(order) {
            return _.first(order.status).id == 'appointment-completed';
        }

        function isQuoteCompleted(quote) {
            return _.first(quote.status).id == 'quote-completed';
        }

        function isQuotePending(quote) {
            return _.first(quote.status).id == 'quote-pending';
        }
    }
})();
