(function() {
    'use strict';

    angular
        .module('app.pages.home', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider) {
        // State
        $stateProvider
            .state('app.home', {
                url: '/',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/pages/home/home.html',
                        controller: 'HomeController as vm',
                        resolve: {
                            User: function(AuthService) {
                                return AuthService.firebaseAuthObject.$requireSignIn();
                            },
                            Quotes: function (QuoteService) {
                                return QuoteService.getQuotes();
                            },
                            OrdersTM: function(OrderService) {
                                return OrderService.getOrdersThisMonth().$loaded();
                            },
                            OrdersTW: function(OrderService) {
                                return OrderService.getOrdersThisWeek().$loaded();
                            },
                            OrdersLW: function(OrderService) {
                                return OrderService.getOrdersLastWeek().$loaded();
                            },
                            Analytics: function(AnalyticsService) {
                                return AnalyticsService.getAnalytics().$loaded();
                            }
                        }
                    }
                }
            });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/pages/home');

        msNavigationServiceProvider.saveItem('geral', {
            title: 'Geral',
            icon: 'icon-home',
            state: 'app.home',
            weight: 0
        });
    }
})();
