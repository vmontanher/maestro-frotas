(function() {
    'use strict';

    angular
        .module('app.pages.quotes')
        .controller('QuotesController', QuotesController);

    /** @ngInject */
    function QuotesController($state, $mdDialog, Quotes, Toast, QuoteService, DataTablePTBR) {
        var vm = this;

        // Data
        vm.quotes = Quotes;
        vm.dtInstance = {};

        //Methods
        vm.goToQuote = goToQuote;
        vm.isQuoteCompleted = isQuoteCompleted;

        init();

        //////////////

        function init() {
            loadQuotesTable();
        }

        function loadQuotesTable() {

            vm.dtOptions = {
                dom: 'rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
                columnDefs: [{
                    // Target the id column
                    targets: 0,
                    width: '150px'
                }, {
                    targets: [0, 5],
                    responsivePriority: 1
                }, {
                    targets: 3,
                    sortable: true,
                    render: function(data, type) {
                        if (type === 'display') {
                            var orderStatus = getQuoteStatus(JSON.parse(data));
                            return '<span class="status ' + orderStatus.color + '">' + orderStatus.name + '</span>';
                        }

                        if (type === 'filter') {
                            return getQuoteStatus(JSON.parse(data)).name;
                        }

                        return data;
                    }
                }, {
                    targets: 4,
                    type: 'date'
                }, {
                    // Target the actions column
                    targets: 5,
                    filterable: false,
                    sortable: false
                }],
                initComplete: function() {
                    var api = this.api(),
                        searchBox = angular.element('body').find('#quotes-search');

                    // Bind an external input as a table wide search box
                    if (searchBox.length > 0) {
                        searchBox.on('keyup', function(event) {
                            api.search(event.target.value).draw();
                        });
                    }
                },
                pagingType: 'simple',
                order: [4, 'desc'],
                lengthMenu: [10, 20, 30, 50, 100],
                pageLength: 20,
                responsive: true,
                language: DataTablePTBR,
                scrollY: 'auto'
            };
        }

        function goToQuote(id) {
            $state.go('app.quotes.detail', { id: id });
        }

        function getQuoteStatus(statuses) {
            return _.first(statuses);
        }

        function isQuoteCompleted(quote) {
            return _.first(quote.status).id == 'quote-completed';
        }

        function deleteQuote(quote, ev) {
            var confirm = $mdDialog.confirm()
                .title('Tem certeza que deseja remover esse orçamento?')
                .textContent('Essa ação não pode ser revertida.')
                .ariaLabel('remove quote')
                .targetEvent(ev)
                .ok('Remover')
                .cancel('Cancelar');

            $mdDialog.show(confirm).then(function() {
                QuoteService.removeQuote(quote).then(function() {
                    Toast.show('Orçamento removido');
                }, function(error) {
                    Toast.show('Erro ao remover orçamento');
                });
            });
        }



    }

})();
