(function() {
    'use strict';

    angular
        .module('app.pages.quotes', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider) {

        // State
        $stateProvider.state('app.quotes', {
            url: '/quotes',
            views: {
                'content@app': {
                    templateUrl: 'app/main/pages/quotes/quotes.html',
                    controller: 'QuotesController as vm',
                    resolve: {
                        User: function(AuthService) {
                            return AuthService.firebaseAuthObject.$requireSignIn();
                        },
                        Quotes: function(QuoteService) {
                            return QuoteService.getQuotes().$loaded();
                        }
                    }
                }
            }
        });

        msNavigationServiceProvider.saveItem('quotes', {
            title: 'Orçamentos',
            icon: 'icon-clipboard-text',
            state: 'app.quotes',
            weight: 2
        });


    }

})();
